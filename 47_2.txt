Module-1:         

Fundamentals: Basic definition of sequential circuit, block diagram, mathematical representation, 
concept of transition table and transition diagram (Relating of Automata concept to sequential 
circuit concept). 
Design of sequence detector, Introduction to finite state model  
Finite state machine: Definitions, capability & state equivalent, kth- equivalent concept 
Merger graph, Merger table, Compatibility graph  
Finite memory definiteness, testing table & testing graph.  
Deterministic finite automaton and non deterministic finite automaton.  
Transition diagrams and Language recognizers. 
Finite Automata: NFA with I transitions - Significance, acceptance of languages. 
Conversions and Equivalence: Equivalence between NFA with and without I transitions. NFA to DFA 
conversion. 
Minimization of FSM, Equivalence between two FSM’s , Limitations of FSM  
Application of finite automata, Finite Automata with output- Moore & Melay machine. 



Module-2:   
 
Regular Languages : Regular sets. 
Regular expressions, identity rules. Arden’s theorem state and prove
Constructing finite Automata for a given regular expressions, Regular string accepted by NFA/DFA 

Pumping lemma of regular sets. Closure properties of regular sets (proofs not required). 
Grammar Formalism: Regular grammars-right linear and left linear grammars.  
Equivalence between regular linear grammar and FA.
Inter conversion, Context free grammar. 
Derivation trees, sentential forms. Right most and leftmost derivation of strings. (Concept only) 


Module-3:      

Context Free Grammars, Ambiguity in context free grammars.
Minimization of Context Free Grammars. 
Chomsky normal form and Greibach normal form. 
Pumping Lemma for Context Free Languages.  
Enumeration of properties of CFL (proofs omitted). Closure property of CFL, Ogden’s lemma & its 
applications  
Push Down Automata: Push down automata, definition. 
Acceptance of CFL, Acceptance by final state and acceptance by empty state and its equivalence. 
Equivalence of CFL and PDA, interconversion. (Proofs not required). 
Introduction to DCFL and DPDA.